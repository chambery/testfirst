library clazz;

class Class
{
  static final Map<String, Class> _classes = {};
  String name;
  String shortname;
  Class(this.name, this.shortname) 
  {
    _classes[name] = this;
  }
  
  static Class get(name)
  {
    return _classes[name];
  }
}