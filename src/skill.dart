library resource;

import 'class.dart';
import 'ability.dart';
import 'dart:mirrors';

abstract class Resource {
  String name;
  String description;
  Resource(this.name, this.description);
  
  Resource.map(Map data)
  {
    ObjectMirror o = reflect(this);
    ClassMirror c = reflectClass(runtimeType);
    while(c != null)
    {
      for (var k in c.declarations.keys) {
        if(data[MirrorSystem.getName(k)] != null)
        {
          print('\tsetting ${k}');
          o.setField(k, data[MirrorSystem.getName(k)]);        
        }
      }
      c = c.superclass;
    }
    _getDb()[name] = this;
  }
  
  Map _getDb();
}

class Skill extends Resource
{
  /* skill 'database' */
  static final Map<String, Skill> _skills = {};

  List<Class> classes;
  bool untrained;
  String detail; 
  Map<String, Skill> _subtypes = {};
  Skill parent;
  String ability;
  
  Skill(name, description, this.classes, this.ability) : super(name, description)
  {
//    this.ability = ability; 
    if(classes == null)
    {
      classes = [];
    }
    _skills[name] = this;
  }
  
  Skill.map(Map data) : super.map(data);
  
  get subtypes => _subtypes;
  
  set subtypes(List<Map> subskills)
  {
    for(Map subSkillData in subskills)
    {
      Skill subskill = new Skill.map(subSkillData);
      subskill.ability = this.ability;
      _subtypes[subskill.name] = subskill;
      subskill.parent = this;
      _skills[subskill.name] = subskill;
    }
  }
  
//  get ability => ability;
//  set ability(name) => Ability.get(name);
  
  static Skill get(name)
  {
    return _skills[name];
  }
  
  Map _getDb()
  {
    return _skills;
  }
}

