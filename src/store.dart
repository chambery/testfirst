library store;

import 'package:darmatch/io_matchers.dart';
import 'package:darmatch/matchers.dart';
import 'package:darmatch/preconditions.dart';
import 'armor.dart';
import 'class.dart';
import 'skill.dart';
import 'dart:mirrors';
import 'dart:io';
import 'package:yaml/yaml.dart';

Map load()
{
  var file = new File('../src/resources/skills.yaml');
  String skillsData = file.readAsStringSync();
  List skills = loadYaml(skillsData);
  for(var skill in skills)
  {
    new Skill.map(skill);
  }
  
}