library resourcetest;

import 'package:unittest/unittest.dart';
import 'package:unittest/vm_config.dart';
import '../src/armor.dart';
import '../src/skill.dart';
import '../src/ability.dart';
import '../src/class.dart';
import '../src/common.dart';
import 'package:yaml/yaml.dart';
import 'dart:io';
import 'dart:async';


void main() {
  useVMConfiguration();

  test('constructor', () {
    
    Skill skill = new Skill.map({
                                  'name': 'foo', 
                                  'description': 'bar',
                                  'baz' : 'qux',
//                                  'ability' : 'Strength',
                                  'subtypes' : [
                                                  { 'name' : 'foosub1', 
                                                    'description' : 'foosub1',
                                                    'classes' : [Class.get('Fighter'), Class.get('Sorcerer')]
                                                  }
                                               ]
                                               
                                });
    expect(skill, isNotNull);
    expect(skill.name, 'foo');
    Skill foo = Skill.get('foo');
    expect(foo, isNotNull);
    expect(foo.description, 'bar');
    expect(foo.subtypes.keys.length, 1);
    expect(foo.subtypes['foosub1'], isNotNull);
    expect(foo.subtypes['foosub1'].name, 'foosub1');
//    expect(foo.ability, Ability.STRENGTH);
  });
}