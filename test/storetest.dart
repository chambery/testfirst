library storetest;

import '../src/store.dart';
import 'package:yaml/yaml.dart';
import 'dart:io';
import '../src/skill.dart';

import 'package:unittest/unittest.dart';
import 'package:unittest/vm_config.dart';

void main()
{
  useVMConfiguration();
  
  test('load', () {
    load();
    Skill skill = Skill.get("Acrobatics");
    expect(skill, isNotNull);
  });
}